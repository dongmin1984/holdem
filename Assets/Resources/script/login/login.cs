﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using GameDevWare.Serialization;
using Newtonsoft.Json.Linq;//use Nuget to get Newtonsoft package
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class login : MonoBehaviour
{

    public GameObject ID;
    public GameObject PW;

    public bool isScene = false;
    // Start is called before the first frame update
    void Start()
    {
        var network = networkManager.Instance;

    }

    // Update is called once per frame
    void Update()
    {

        if (isScene == true) {
            isScene = false;
            SceneManager.LoadScene("lobby");
        }
    }

    public void ReceiveSuccess(string data) 
    {

        if (protocolreceive.IsError(data))
        {
            Debug.Log(data);
        }
        else 
        {
                isScene = true;

            infoManager.Instance.mainUser = protocolreceive.ConvertToUser(data);
        }

    }


    public void OnClickLogin() {

        string userID = "";
        string userPW = "";
        foreach (var text in ID.GetComponentsInChildren<InputField>())
        {
            userID = text.text;
        }
        foreach (var text in PW.GetComponentsInChildren<InputField>())
        {
            //if (text.name == "Text")
            {
                userPW = text.text;
            }
        }

        
        networkManager.Instance.Send("login",protocol.login(userID, userPW), ReceiveSuccess);


    }
}

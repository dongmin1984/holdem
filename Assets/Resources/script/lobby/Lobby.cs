﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;
using System;

public class Lobby : MonoBehaviour
{
    public GameObject preRoom;
    public GameObject roomContent;
    public bool isLoad = false;
    public bool isScenes = false;
    // Start is called before the first frame update

   

    public void ReceiveUserInfo(string data)
    {
        //dynamic result = JObject.Parse(data);

        //if (result.error != null)
        //{
        //    Debug.Log(result.error);
        //}
        //else
        //{
        //    //infoManager.Instance.mainUser = protocolreceive.ConvertToUser(data);
        //}

    }

    public void ReceiveRooms(string data)
    {
        //dynamic result = JObject.Parse(data);

        //if (result.error != null)
        //{
        //    return;
        //}

        //infoManager.Instance.rooms = protocolreceive.ConvertToRoom(data);

        //Debug.Log("ReceiveRooms");

        //foreach (var gameroom in result.data.rooms) {


        //    Room room = new Room();
        //    room.id = gameroom.id.ToString();
        //    room.name = gameroom.name.ToString();
        //}


        infoManager.Instance.rooms = protocolreceive.ConvertToRoom(data);

        isLoad = true;
        Debug.Log("isLoad");

        //foreach(var  room in infoManager.Instance.rooms)
        {
            //for (int i = 0; i < 100; i++) {
            //    GameObject clone = UnityEngine.Object.Instantiate(this.preRoom, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            //    //clone.transform.localPosition = playerPos[i];
            //    clone.name = "room_" + i;

            //    clone.transform.SetParent(this.roomContent.transform);
            //}

            //roomContent.
            //.Add(clone);
        }

    }





    void Start()
    {
        networkManager.Instance.Send("userInfo", protocol.userinfo(), ReceiveUserInfo);

        networkManager.Instance.Send("getRooms", protocol.getRooms(), ReceiveRooms);

    }

    // Update is called once per frame
    void Update()
    {

        if (isScenes)
        {
            isScenes = false;

            UnityEngine.SceneManagement.SceneManager.LoadScene("game2");

        } 

        if (isLoad)
        {
            isLoad = false;

            foreach(var room in infoManager.Instance.rooms)
            {

                Debug.Log(room.name);

                GameObject clone = UnityEngine.Object.Instantiate(this.preRoom, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                clone.name = "room_" + room.id;



                clone.transform.SetParent(this.roomContent.transform);

                clone.GetComponent<Button>().onClick.AddListener(() => {

                    infoManager.Instance.joinRoomIdx = room.id;
                    networkManager.Instance.Send("enterRoom", protocol.enterRoom(infoManager.Instance.joinRoomIdx), (string data) => {


                        infoManager.Instance.joinRoom = protocolreceive.ConvertToJoinRoom(data, infoManager.Instance.mainUser.account);


                        isScenes = true;
                    });
                });
            }   
        }

     
    }
}

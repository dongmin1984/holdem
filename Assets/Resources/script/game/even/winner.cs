﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace even
{
    public class winner : even
    {
        public winner()
        {
        }


        public override void Do()
        {

            if (infoManager.Instance.joinRoom.winner != null)
            {

                tableUser user = infoManager.Instance.joinRoom.slots[infoManager.Instance.joinRoom.winner.account];


                player player = playerManager.Instance.players[user.gameNumber].GetComponent<player>();

                player.finishMark.SetActive(true);

                playerManager.Instance.StartCoroutine(CloseWinner(user.gameNumber));


            }
        }


        IEnumerator CloseWinner(int gameNumber)
        {
            yield return new WaitForSeconds(2);

            player player = playerManager.Instance.players[gameNumber].GetComponent<player>();
            player.finishMark.SetActive(false);


        }
    }
}

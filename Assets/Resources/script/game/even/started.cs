﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace even
{
    public class started : even

    {
        public started()
        {
        }


        public override void Do()
        {
            foreach (var player in playerManager.Instance.players)
            {
                if (player.active == true) 
                {
                    player userOBJ = player.GetComponent<player>();
                    userOBJ.playButton.SetActive(false);

                    if (userOBJ.cardEffect.Length > 0) {
                        userOBJ.cardEffect[0].SetActive(false);
                        userOBJ.cardEffect[1].SetActive(false);
                    }

                }
            }

            var  dealer = infoManager.Instance.joinRoom.slots.First(s => s.Value.chairNumber == infoManager.Instance.joinRoom.dealerSlot).Value;

            if (dealer != null) 
            {
                player userOBJ = playerManager.Instance.players[dealer.gameNumber].GetComponent<player>();
                userOBJ.playButton.SetActive(true);
            }





        }
    }
}

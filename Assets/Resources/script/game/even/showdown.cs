﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace even
{
    class showdown : even
    {
        public showdown()
        {
        }


        public override void Do()
        {


            foreach (var player in playerManager.Instance.players)
            {
                var user = player.GetComponent<player>();

                if (user.userCard1 != null && user.userCard2 != null)
                {
                    var test1 = Sprite.Create(resourcesManager.Instance.FindTexture("00.png"), new Rect(0, 0, 122, 162), new Vector2(0.5f, 0.5f));


                    var card1 = user.userCard1.GetComponent<Image>();

                    card1.sprite = test1;


                    var card2 = user.userCard2.GetComponent<Image>();
                    card2.sprite =
                            Sprite.Create(resourcesManager.Instance.FindTexture("00.png"), new Rect(0, 0, 122, 162), new Vector2(0.5f, 0.5f));

                    user.userCard1.SetActive(false);
                    user.userCard2.SetActive(false);

                    if (user.cardEffect.Length >= 2) {
                        user.cardEffect[0].SetActive(false);
                        user.cardEffect[1].SetActive(false);

                    }



                }

            }

            GameObject[] tableCard = playerManager.Instance.tableCard;
            GameObject[] effectCard = playerManager.Instance.cardEffect;

            for (var index = 0; index < tableCard.Length; index++)
            {
                if (tableCard[index].active)
                {


                    var card2 = tableCard[index].GetComponent<Image>();
                    card2.sprite =
                            Sprite.Create(resourcesManager.Instance.FindTexture("00.png"), new Rect(0, 0, 122, 162), new Vector2(0.5f, 0.5f));

                    tableCard[index].SetActive(false);
                    effectCard[index].SetActive(false);
                    

                }
            }



            //UserCard[] userCards = playerManager.Instance.players;

        }
    }
}

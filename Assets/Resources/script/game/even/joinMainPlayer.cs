﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace even
{
    public class joinMainPlayer : even
    {
        Transform parent;
        GameObject prefab;
        String prefabname;
        Vector3 playerPos;
        public joinMainPlayer(string _prefabname, GameObject _prefab, Transform _parent, Vector3 _playerPos)
        {
            prefab = _prefab;
            parent = _parent;
            playerPos = _playerPos;
            prefabname = _prefabname;
        }


        public override void Do()
        {
            GameObject clone = UnityEngine.Object.Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity, parent) as GameObject;

            clone.transform.localPosition = playerPos;
            clone.name = prefabname;
            //players.Add(clone);
        }
    }
}

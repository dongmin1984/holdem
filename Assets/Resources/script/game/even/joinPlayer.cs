﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace even
{
    public class joinPlayer : even
    {

        int playNumber;
        public joinPlayer(int _playNumber)
        {
            playNumber  = _playNumber;
        }


        public override void Do()
        {


            if (playerManager.Instance.players.Length <= playNumber) {

                Debug.LogWarning("playerManager.Instance.players");
                return;
            }


            playerManager.Instance.players[playNumber].SetActive(true);

            player user = playerManager.Instance.players[playNumber].GetComponent<player>();

            tableUser info = infoManager.Instance.joinRoom.slots.ToList().Find(s => s.Value.gameNumber == playNumber).Value;

            user.SetName(info.account);
            user.SetBalance(string.Format("{0:C0}", info.money) );

            if (info.isWait()) 
            {
                user.userCard1.SetActive(false);
                user.userCard2.SetActive(false);



                user.cardEffect[0].SetActive(false);
                user.cardEffect[1].SetActive(false);

            }



        }
    }
}

﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIAnimatorCore;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace even
{

    public class dealerCard : even
    {

        public dealerCard()
        {
        }


        public override void Do()
        {

            foreach (var slot in infoManager.Instance.joinRoom.slots)
            {

                var user = playerManager.Instance.players[slot.Value.gameNumber].GetComponent<player>();
                if (user != null) 
                {
                    user.userCard1.SetActive(true);
                    user.userCard2.SetActive(true);
                }


                var ani  = playerManager.Instance.dealerCard[slot.Value.gameNumber].GetComponent<UIAnimator>();
                ani.PlayAnimation(AnimSetupType.Intro);

            }

            playerManager.Instance.StartCoroutine(OpenCard());
        }


        IEnumerator OpenCard()
        {
            yield return new WaitForSeconds(1.3f);


            UserCard[] tableCard = infoManager.Instance.joinRoom.userCard;

            if (tableCard[0] != null) 
            {
                var user = playerManager.Instance.players[0].GetComponent<player>();
                
                string filename1 = Utility.valueToString(tableCard[0].card1.value) + Utility.suitToString(tableCard[0].card1.suit);

                var card1 = user.userCard1.GetComponent<Image>();

                card1.sprite =
                        Sprite.Create(resourcesManager.Instance.FindTexture(filename1 + ".png"), new Rect(0, 0, 122, 162), new Vector2(0.5f, 0.5f));

                string filename2 = Utility.valueToString(tableCard[0].card2.value) + Utility.suitToString(tableCard[0].card2.suit);

                var card2 = user.userCard2.GetComponent<Image>();
                card2.sprite =
                        Sprite.Create(resourcesManager.Instance.FindTexture(filename2 + ".png"), new Rect(0, 0, 122, 162), new Vector2(0.5f, 0.5f));

            }

        }
    }
}

﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace even
{
    public class OpenUserCard : even
    {
        String account;
        String first;
        String second;
        
        public OpenUserCard(string _account, string _first, string _second)
        {
            account = _account;
            second = _second;
            first = _first;
            
        }


        public override void Do()
        {

            if (infoManager.Instance.joinRoom.slots.ContainsKey(account))
            {
                tableUser user = infoManager.Instance.joinRoom.slots[account];


                player player = playerManager.Instance.players[user.gameNumber].GetComponent<player>();

                var userCard1 = player.userCard1.GetComponent<Image>();
                var userCard2 = player.userCard2.GetComponent<Image>();

                userCard1.sprite =
                    Sprite.Create(resourcesManager.Instance.FindTexture(first + ".png"), userCard1.sprite.rect, userCard1.sprite.pivot);
                
                userCard2.sprite =  
                    Sprite.Create(resourcesManager.Instance.FindTexture(second + ".png"), userCard2.sprite.rect, userCard2.sprite.pivot);

            }

        }
    }
}

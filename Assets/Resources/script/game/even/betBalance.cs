﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;

namespace even
{
    public class betBalance : even
    {
        public string account;
        public betBalance(string _account)
        {
            account = _account;
        }


        public override void Do()
        {
            if (infoManager.Instance.joinRoom.slots.ContainsKey(account)) 
            {
                var tableUser = infoManager.Instance.joinRoom.slots[account];


                var player = playerManager.Instance.players[tableUser.gameNumber].GetComponent<player>();

                player.userBalance.GetComponent<Text>().text = string.Format("{0:C0}", tableUser.money);
                player.userBetBalance.GetComponent<Text>().text = string.Format("{0:C0}", tableUser.betBalance);



            }


        }

    }
}

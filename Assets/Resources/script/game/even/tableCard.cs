﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIAnimatorCore;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace even
{
    public class tableCard : even
    {
        String state;
        public tableCard()
        {
        }


        public override void Do()
        {
            Card[] infoTableCard = infoManager.Instance.joinRoom.tableCards;
            GameObject[] tableCard = playerManager.Instance.tableCard;
            GameObject[] effectCard = playerManager.Instance.cardEffect;
            for (var index = 0; index < infoTableCard.Length; index++)
            {
                if (infoTableCard[index] != null)
                {
                    if (!tableCard[index].active)
                    {

                        tableCard[index].SetActive(true);

                        var ani = playerManager.Instance.animationTabelCard[index].GetComponent<UIAnimator>();
                        ani.PlayAnimation(AnimSetupType.Intro);

                        string filename = Utility.valueToString(infoTableCard[index].value) + Utility.suitToString(infoTableCard[index].suit);

                        
                        playerManager.Instance.StartCoroutine(OpenTableCard(index, filename));

                    }
                }
            }

        }

        IEnumerator OpenTableCard(int index, string filename)
        {
            yield return new WaitForSeconds(0.6f);


            playerManager.Instance.tableCard[index].GetComponent<Image>().sprite =

                 Sprite.Create(resourcesManager.Instance.FindTexture(filename + ".png"), new Rect(0, 0, 122, 162), new Vector2(0.5f, 0.5f));
         



        }
    }
}

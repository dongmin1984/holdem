﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
//using UnityEditorInternal;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class eventManager : MonoBehaviour
{


    public Queue<even.even> eventQueue = new Queue<even.even>();





    private static eventManager _instance;
    public static eventManager Instance
    {
        get
        {
            if (_instance == null)
            {
          

                var newSingleton = new GameObject("eventManager");
                _instance =  newSingleton.AddComponent<eventManager>();
            }

            return _instance;
        }
    }


    void Start()
    {
        //this.setState(new wait());
    }

    // Update is called once per frame
    void Update()
    {


        if (eventQueue.Count > 0) {

            var ms = eventQueue.Dequeue();
            ms.Do();
        }


    }
}

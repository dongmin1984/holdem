﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    enum Suit
    {
        CLUBS = 0,
        DIAMONDS = 1,
        HEARTS = 2,
        SPADES = 3,
    }

    enum Value
    {
        ACE = 0,
        TWO = 1,
        THREE = 2,
        FOUR = 3,
        FIVE = 4,
        SIX = 5,
        SEVEN = 6,
        EIGHT = 7,
        NINE = 8,
        TEN = 9,
        JACK = 10,
        QUEEN = 11,
        KING = 12,
    }



    class Utility
    {

    public static string suitToString(int suit)
        {
            switch ((Suit)suit)
            {
                case Suit.CLUBS:
                    return "c";
                case Suit.DIAMONDS:
                    return "d";
                case Suit.HEARTS:
                    return "h";
                case Suit.SPADES:
                    return "s";
            }

            return " ";
        }

        public  static string valueToString(int value)
        {
            switch ((Value)value)
            {
                case Value.ACE:
                    return "a";
              case Value.TWO:
                    return "2";
                case Value.THREE:
                    return "3";
                case Value.FOUR:
                    return "4";
                case Value.FIVE:
                    return "5";
                case Value.SIX:
                    return "6";
                case Value.SEVEN:
                    return "7";
                case Value.EIGHT:
                    return "8";
                case Value.NINE:
                    return "9";
                case Value.TEN:
                    return "t";
                case Value.JACK:
                    return "j";
                case Value.QUEEN:
                    return "q";
                case Value.KING:
                    return "k";
            }

            return "";
        }

    }

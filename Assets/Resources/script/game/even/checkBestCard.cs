﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace even
{
    class checkBestCard: even

    {


        public override void Do()
        {

            tableUser mainUser = infoManager.Instance.joinRoom.slots[infoManager.Instance.mainUser.account];
            if (mainUser.isWait()) {
                return;
            }

            Room room = infoManager.Instance.joinRoom;
            
            Holdem.Hand hand = new Holdem.Hand();


            UserCard card = infoManager.Instance.joinRoom.userCard[0];

            hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(card.card1.value), Holdem.Card.ServerSultToClient(card.card1.suit)));
            hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(card.card2.value), Holdem.Card.ServerSultToClient(card.card2.suit)));

            if (room.IsPreflop())
            {
                if (Holdem.HandCombination.isOnePair(hand))
                {
                    player player = playerManager.Instance.players[0].GetComponent<player>();
                    player.cardEffect[0].SetActive(true);
                    player.cardEffect[1].SetActive(true);
                }
            }
            else
            {

                for (int i = 0; i < room.tableCards.Length; i++)
                {
                    if (room.tableCards[i] != null)
                    {
                        hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(room.tableCards[i].value), 
                            Holdem.Card.ServerSultToClient(room.tableCards[i].suit)));

                    }
                }

                //hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(2), Holdem.Card.ServerSultToClient(2)));
                //hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(3), Holdem.Card.ServerSultToClient(2)));
                //hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(4), Holdem.Card.ServerSultToClient(2)));
                //hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(5), Holdem.Card.ServerSultToClient(2)));
                //hand.Add(new Holdem.Card(Holdem.Card.ServerRankToClient(6), Holdem.Card.ServerSultToClient(2)));


                var resultHand = Holdem.HandCombination.getBestHand(hand);

                List<int> value = resultHand.getValue();

                Debug.Log(resultHand.ToString());

                if (resultHand.isPair() == true || resultHand.isThree() == true)
                {
                    if (infoManager.Instance.joinRoom.userCard[0].card1.value == Holdem.Card.ClientToServerRank(value[1]))
                    {
                        var player = playerManager.Instance.players[0].GetComponent<player>();
                        player.cardEffect[0].SetActive(true);
                    }

                    if (infoManager.Instance.joinRoom.userCard[0].card2.value == Holdem.Card.ClientToServerRank(value[1]))
                    {
                        var player = playerManager.Instance.players[0].GetComponent<player>();
                        player.cardEffect[1].SetActive(true);
                    }

                    for (int i = 0; i < 5; i++)
                    {
                        if (
                            i< infoManager.Instance.joinRoom.tableCards.Length  &&
                            infoManager.Instance.joinRoom.tableCards[i] != null &&
                            infoManager.Instance.joinRoom.tableCards[i].value == Holdem.Card.ClientToServerRank(value[1]))
                        {
                            Debug.Log(infoManager.Instance.joinRoom.tableCards[i].value + "  , " + Holdem.Card.ClientToServerRank(value[1]));
                            playerManager.Instance.cardEffect[i].SetActive(true);
                        }
                    }

                }
                else if (resultHand.isTwo() == true || resultHand.isFull() == true)
                {
                    if (infoManager.Instance.joinRoom.userCard[0].card1.value == Holdem.Card.ClientToServerRank(value[1]) 
                        || infoManager.Instance.joinRoom.userCard[0].card1.value == Holdem.Card.ClientToServerRank(value[2]))
                    {
                        var player = playerManager.Instance.players[0].GetComponent<player>();
                        player.cardEffect[0].SetActive(true);
                    }

                    if (infoManager.Instance.joinRoom.userCard[0].card2.value == Holdem.Card.ClientToServerRank(value[1]) || 
                        infoManager.Instance.joinRoom.userCard[0].card2.value == Holdem.Card.ClientToServerRank(value[2]))
                    {
                        var player = playerManager.Instance.players[0].GetComponent<player>();
                        player.cardEffect[1].SetActive(true);
                    }

                    for (int i = 0; i < 5; i++)
                    {
                        if (
                            i < infoManager.Instance.joinRoom.tableCards.Length &&
                                                        infoManager.Instance.joinRoom.tableCards[i] != null &&

                            (infoManager.Instance.joinRoom.tableCards[i].value == Holdem.Card.ClientToServerRank(value[1]) ||
                            infoManager.Instance.joinRoom.tableCards[i].value == Holdem.Card.ClientToServerRank(value[2])))
                        {
                            playerManager.Instance.cardEffect[i].SetActive(true);
                        }
                    }
                }
                else if (resultHand.isFlush())
                 {   
                    var suit = hand.myHand.GroupBy(info => info.suit).Select(s => new { suit = s.Key, count = s.Count() }).OrderByDescending (s => s.count).Select(s => s.suit).FirstOrDefault();

                    if (Holdem.Card.ServerSultToClient(infoManager.Instance.joinRoom.userCard[0].card1.suit) == suit)
                    {
                        var player = playerManager.Instance.players[0].GetComponent<player>();
                        player.cardEffect[0].SetActive(true);
                    }

                    if (Holdem.Card.ServerSultToClient(infoManager.Instance.joinRoom.userCard[0].card2.suit) == suit)
                    {
                        var player = playerManager.Instance.players[0].GetComponent<player>();
                        player.cardEffect[1].SetActive(true);
                    }

                    for (int i = 0; i < 5; i++)
                    {
                        if (i < room.tableCards.Length &&
                            room.tableCards[i] != null &&
                            Holdem.Card.ServerSultToClient(room.tableCards[i].suit) == suit)
                        {
                            playerManager.Instance.cardEffect[i].SetActive(true);
                        }
                    }

                }
                else if (resultHand.isStraight())
                { 

                     List<Holdem.Card> cards = hand.myHand.OrderByDescending(s => s.rank ).ToList();
                    List<int> straight = new List<int>();
                    
                    for(int i = 0; i < cards.Count - 1;i++) 
                    {
                        if (cards[i].rank == cards[i + 1].rank + 1)
                        {
                            straight.Add(cards[i].rank);
                        }
                        else
                        {
                            if (straight.Count >= 5){ break; }
                            straight.Clear();
                        }
                    }

                    for (int i = 0; i < straight.Count; i++)
                    {
                        if (infoManager.Instance.joinRoom.userCard[0].card1.value == Holdem.Card.ClientToServerRank(straight[i]))
                        {
                            var player = playerManager.Instance.players[0].GetComponent<player>();
                            player.cardEffect[0].SetActive(true);
                        }

                        if (infoManager.Instance.joinRoom.userCard[0].card2.value == Holdem.Card.ClientToServerRank(straight[i]))
                        {
                            var player = playerManager.Instance.players[0].GetComponent<player>();
                            player.cardEffect[1].SetActive(true);
                        }
                        
                        if ( i < room.tableCards.Length &&
                             room.tableCards[i] != null &&
                             Holdem.Card.ServerSultToClient(room.tableCards[i].suit) == straight[i])
                        {
                            playerManager.Instance.cardEffect[i].SetActive(true);
                        }
                    }

                }
            }

      
        }

    }
}

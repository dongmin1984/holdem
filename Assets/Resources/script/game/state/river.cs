﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class river : stateBase
{

    public static stateBase Create(string data)
    {
        return new river(data);
    }

    river(string date)
        : base(date)
    {
        base.name = "river";

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public override void begin(playerManager mgr)
    {
        QuickType.PreFlop.Temperatures ms = JsonConvert.DeserializeObject<QuickType.PreFlop.Temperatures>(data);
        
        infoManager.Instance.joinRoom.state = ms.Data.RoomState;


        if (ms.Data.TableCard5 != null)
        {

            infoManager.Instance.joinRoom.tableCards[4] = new TableCard()
            {
                value = ms.Data.TableCard5.Value,
                suit = ms.Data.TableCard5.Suit,
                numericValue = ms.Data.TableCard5.NumericValue,
            };



            eventManager.Instance.eventQueue.Enqueue(new even.tableCard());
            eventManager.Instance.eventQueue.Enqueue(new even.checkBestCard());

        }
        //Card[] card = new Card[3]();


    }
}

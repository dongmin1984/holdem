﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
public class flop : stateBase
{

    public static stateBase Create(string data) 
    {
        return new flop(data);
    }

    flop(string date) 
        : base(date)
    {
        base.name = "flop";

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public override void begin(playerManager mgr)
    {
        QuickType.PreFlop.Temperatures ms = JsonConvert.DeserializeObject<QuickType.PreFlop.Temperatures>(data);
        
        infoManager.Instance.joinRoom.state = ms.Data.RoomState;


        if (ms.Data.TableCard1 != null)
        {
            infoManager.Instance.joinRoom.tableCards = new TableCard[5];

            infoManager.Instance.joinRoom.tableCards[0] = new TableCard()
            {
                value = ms.Data.TableCard1.Value,
                suit = ms.Data.TableCard1.Suit,
                numericValue = ms.Data.TableCard1.NumericValue,
            };

            infoManager.Instance.joinRoom.tableCards[1] = new TableCard()
            {
                value = ms.Data.TableCard2.Value,
                suit = ms.Data.TableCard2.Suit,
                numericValue = ms.Data.TableCard2.NumericValue,
            };

            infoManager.Instance.joinRoom.tableCards[2] = new TableCard()
            {
                value = ms.Data.TableCard3.Value,
                suit = ms.Data.TableCard3.Suit,
                numericValue = ms.Data.TableCard3.NumericValue,
            };


            eventManager.Instance.eventQueue.Enqueue(new even.tableCard());
            eventManager.Instance.eventQueue.Enqueue(new even.checkBestCard());


        }
        //Card[] card = new Card[3]();


    }
}

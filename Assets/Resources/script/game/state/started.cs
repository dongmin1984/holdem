﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

public class started : stateBase
{

    public static stateBase Create(string data)
    {
        return new started(data);
    }

    started(string date)
        : base(date)
    {
        base.name = "started";

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void begin(playerManager mgr)
    {
        QuickType.GameStart.Temperatures ms = JsonConvert.DeserializeObject<QuickType.GameStart.Temperatures>(data);

        if (ms.Data != null) {

            infoManager.Instance.joinRoom.dealerSlot = ms.Data.DealerSlot;

            infoManager.Instance.joinRoom.slots.Values.ToList().ForEach(s => { s.state = "gaming"; });
            
        }

        //Card[] card = new Card[3]();

    }

    public override void end(playerManager mgr)
    {
        eventManager.Instance.eventQueue.Enqueue(new even.started());
;


        //GameObject[] tableCard = playerManager.Instance.tableCard;
        //for (var index = 0; index < tableCard.Length; index++)
        //{
        //        if (!tableCard[index].active == true)
        //        {
        //            tableCard[index].SetActive(false);

        //        }
        //}

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class turn : stateBase
{
    public static stateBase Create(string data)
    {
        return new turn(data);
    }

    turn(string date)
        : base(date)
    {
        base.name = "turn";

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void begin(playerManager mgr)
    {
        QuickType.PreFlop.Temperatures ms = JsonConvert.DeserializeObject<QuickType.PreFlop.Temperatures>(data);
        infoManager.Instance.joinRoom.state = ms.Data.RoomState;


        if (ms.Data.TableCard4 != null)
        {

            infoManager.Instance.joinRoom.tableCards[3] = new TableCard()
            {
                value = ms.Data.TableCard4.Value,
                suit = ms.Data.TableCard4.Suit,
                numericValue = ms.Data.TableCard4.NumericValue,
            };



            eventManager.Instance.eventQueue.Enqueue(new even.tableCard());
            eventManager.Instance.eventQueue.Enqueue(new even.checkBestCard());

        }
        //Card[] card = new Card[3]();

    }

}

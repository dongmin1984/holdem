﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;
using QuickType.PreFlop;

public class join : stateBase
{

    public static stateBase Create(string data)
    {
        return new join(data);
    }

    join(string date)
        : base(date)
    {
        base.name = "join";


        //dynamic gameData = JObject.Parse(date);


        //var room =  protocolreceive.ConvertToJoinRoom(date);
        //room.state = gameData.data.state;

        //infoManager.Instance.joinRoom = room;

    }
    // Start is called before the first frame update
    public override void begin(playerManager mgr)
    {

        //for (int i = 0; i < mgr.player.Length; i++) {
        //    mgr.mainPlayer.GetComponent<player>().SendMessage("giveTrump", endPoint[i,0]);
        //}
        //foreach (var play in mgr.player)
        //{
        //    play.GetComponent<player>().SendMessage("outGame");
        //}

        //mgr.mainPlayer.GetComponent<player>().SendMessage("outGame");


    }



    public override void progress(playerManager mgr)
    {

        var  room = infoManager.Instance.joinRoom;

        
        foreach(var slot in room.slots)
        {
            eventManager.Instance.eventQueue.Enqueue(new even.joinPlayer(slot.Value.gameNumber));
        }
        
        infoManager.Instance.joinRoom.tableCards = new TableCard[5];
        infoManager.Instance.joinRoom.userCard = new UserCard[10];
        eventManager.Instance.eventQueue.Enqueue(new even.tableCard());



        mgr.setState(stateBase.MakeState(room.state, ""));
    }


    public override void end(playerManager mgr) { }

}

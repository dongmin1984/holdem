﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using UnityEditor.Experimental.GraphView;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UIAnimatorCore;
using UnityEngine;
using QuickType.Finish;

public class stateBase
{

    public string data;
    public string name;

    public delegate stateBase createState(string even);


    static Dictionary<string, createState> callState = new Dictionary<string, createState>() {

        {"preflop", preflop.Create },
        {"flop", flop.Create },
        {"river", river.Create },
        {"ended", showdown.Create },
        {"turn", turn.Create },
        {"wait", wait.Create },
        {"join", join.Create },
        {"started", started.Create},
        

    };




    public virtual void begin(playerManager mgr) { }
    public virtual void progress(playerManager mgr) { }
    public virtual void end(playerManager mgr) { }


    public stateBase(string _date)  {

        data = _date;
    } 

    public void doBet() 
    { 
 
    }

    public static stateBase MakeState(string name, string data) 
    {

        if (callState.ContainsKey(name)) 
        {
            return callState[name](data);
        }

        return null;
    }


    public void SetMessage(string push, string _ms) 
    {


        if (push == "needBet")
        {
            QuickType.InGame.Temperatures ms = JsonConvert.DeserializeObject<QuickType.InGame.Temperatures>(_ms);

            eventManager.Instance.eventQueue.Enqueue(new even.needBet(ms.Data.User.Account));
        }
        else if (push == "playerBet")
        {
            QuickType.PlayerBet.Temperatures ms = JsonConvert.DeserializeObject<QuickType.PlayerBet.Temperatures>(_ms);

            if (infoManager.Instance.joinRoom.slots.ContainsKey(ms.Data.User.Account))
            {
                infoManager.Instance.joinRoom.slots[ms.Data.User.Account].money = ms.Data.User.Money;
                infoManager.Instance.joinRoom.slots[ms.Data.User.Account].betBalance = ms.Data.betBalance;

                eventManager.Instance.eventQueue.Enqueue(new even.betBalance(ms.Data.User.Account));
            }


            eventManager.Instance.eventQueue.Enqueue(new even.playerBet(ms.Data.User.Account));



    
            


        }
        else if (push == "gameEnded")
        {
            QuickType.Finish.Temperatures ms = JsonConvert.DeserializeObject<QuickType.Finish.Temperatures>(_ms);

            infoManager.Instance.joinRoom.winner = new Winner() {

                id = ms.Data.Winner.Id,
                money = ms.Data.Winner.Money,
                point = ms.Data.Winner.Point,
                savemoney = ms.Data.Winner.Savemoney,
                account = ms.Data.Winner.Account,
            }; 

            foreach (var card in ms.Data.UserCards)
            {
                if (infoManager.Instance.mainUser.account != card.Account) {
                    eventManager.Instance.eventQueue.Enqueue(new even.OpenUserCard(card.Account,
                        Utility.valueToString(card.First.Value) + Utility.suitToString(card.First.Suit),
                        Utility.valueToString(card.Second.Value) + Utility.suitToString(card.Second.Suit)));
                }

            }

            eventManager.Instance.eventQueue.Enqueue(new even.winner());

        }
        

        //if (ms.Push == "needBet") {

        //}


    }

    //public void needBet() 
    //{
    //    UIAnimator animator = playerManager.Instance.betCart.GetComponent<UIAnimator>();
    //    animator.PlayAnimation(AnimSetupType.Intro);

    //    //{ "push":"needBet","data":{ "roomId":3,"roomState":"flop","user":{ "id":4,"account":"test4","password":"1234","money":4000000,"savemoney":1000001,"point":100002} } }
    //    //UnityEngine.Debug:Log(Object)

    //}

}

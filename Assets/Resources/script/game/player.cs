﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour
{

    public GameObject userPictrue;
    public GameObject userTimer;
    public GameObject userName;
    public GameObject userBalance;
    public GameObject userBetBalance;
    public GameObject userBlackGround;

    public GameObject userCard1;
    public GameObject userCard2;
    public GameObject progressBarCircle;
    public GameObject finishMark;
    public GameObject playButton;

    public GameObject[] cardEffect;


    //public GameObject[] playerCard1;


    private void Awake()
    {

        Transform[] objList = this.transform.GetComponentsInChildren<Transform>();

        foreach (Transform child in objList)
        {
            if (child.name == "user_pictrue") {
                userPictrue = child.gameObject;
            }
            else if (child.name == "user_name")
            {
                userName = child.gameObject;

            }
            else if (child.name == "user_balance")
            {
                userBalance = child.gameObject;

            }
            else if (child.name == "progressBarCircle")
            {
                //userTimer = child.gameObject;
                progressBarCircle = child.gameObject;

            }
            else if (child.name == "user_back_groud")
            {
                userBlackGround = child.gameObject;
            }
            else if (child.name == "user_card1")
            {
                userCard1 = child.gameObject;
            }
            else if (child.name == "user_card2")
            {
                userCard2 = child.gameObject;
            }
            else if (child.name == "progressBarCircle")
            {
                progressBarCircle = child.gameObject;
            }
        }

        finishMark.SetActive(false);
    }

    public void StartTimer() 
    {
        userBlackGround.SetActive(false);
        ProgressBarCircle progress = progressBarCircle.GetComponent<ProgressBarCircle>();
        if (progress) 
        {
            progress.Begin();
        }
    }

    public void StopTimer()
    {
        userBlackGround.SetActive(true);
        ProgressBarCircle progress = progressBarCircle.GetComponent<ProgressBarCircle>();
        if (progress)
        {
            progress.End();
        }
    }


    void Start()
    {

        //playerCard1[0].transform.localPosition = new Vector3(-10, 306, 0);
        //playerCard1[1].transform.localPosition = new Vector3(-10, 306, 0);

        //this.StartCoroutine(SomeCoroutine());

    }

    IEnumerator SomeCoroutine()
    {
        yield return new WaitForSeconds(5f);


        //this.startMoveAnimation();
    }



    // Update is called once per frame
    void Update()
    {

    }


    public void SetName(string text)
    {
        Text lable = userName.GetComponent<Text>();
        lable.text = text;
    }

    public void SetBalance(string text)
    {
        Text lable = userBalance.GetComponent<Text>();
        lable.text = text;
    }

    public void ActiveCard(string text)
    {

    }



    //public void inGame()
    //{
    //    this.enabled = true;
    //}

    //public void outGame()
    //{
    //    this.enabled = false;
    //}

    public void giveTrump(Vector3 pos)
    {
        //Debug.Log(playerCard1[0].transform.position);
        //Debug.Log(pos);
        //iTween.MoveTo(playerCard1[0], iTween.Hash("position", pos, "islocal", true, "easetype", iTween.EaseType.easeInQuad, "time", 1f));
        //iTween.MoveTo(playerCard1[1], iTween.Hash("position", pos, "islocal", true, "easetype", iTween.EaseType.easeInQuad, "time", 4f));

        //iTween.MoveFrom(playerCard1[0], iTween.Hash("position", new Vector3(-82.8f, -16.5f, 0), "islocal", true, "time", .6, "easetype", "easeincubic"));
        //iTween.MoveFrom(playerCard1[1], iTween.Hash("position", new Vector3(-82.8f, -16.5f, 0), "islocal", true, "time", .6, "easetype", "easeincubic"));

        //iTween.MoveTo(playerCard1[0], new Vector3(0, -204f, 0), 50f);

        //iTween.MoveTo(playerCard1[0], iTween.Hash(pos, "islocal", false, "time", 1));

        //iTween.MoveTo(playerCard1[1], new Vector2(0, -204f), 1f);
        //iTween.MoveTo(playerCard1[0], iTween.Hash("position", new Vector3(0, -204f, 0), "easetype", iTween.EaseType.linear, "time", 1f));
        //iTween.MoveTo(playerCard1[1], iTween.Hash("position", new Vector3(0, -204f, 0), "easetype", iTween.EaseType.linear, "time", 1f));

        //iTween.MoveTo(gameObject, iTween.Hash("position", nextPos, "time", lobTime + 0.05f, "easeType", iTween.EaseType.linear, "onComplete", "com", "onCompleteTarget", gameObject));
    }

    //public void startMoveAnimation() {

    //    iTween.MoveTo(playerCard1[0], iTween.Hash("position", new Vector3(-2.8f, -1.5f, 0), "easetype", iTween.EaseType.easeInQuad, "time", 1f));
    //    iTween.MoveTo(playerCard1[1], iTween.Hash("position", new Vector3(-2.8f, -1.5f, 0), "easetype", iTween.EaseType.easeInQuad, "time", 1f));

    //}
}

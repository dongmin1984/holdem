﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;
using UIAnimatorCore;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.CodeDom;

public class playerManager : MonoBehaviour
{


    //public GameObject mainPlayer;


    private stateBase state = null;
    // Start is called before the first frame update

    public GameObject[] players;

    public GameObject[] tableCard;

    public GameObject betCart;

    public GameObject[] dealerCard;

    public GameObject[] animationTabelCard;

    public GameObject[] cardEffect;


    public GameObject raisePOP;

    public GameObject animationRaisePOP;

    public GameObject betBalanceView;

    public Slider raiseSlider;
    
    private Boolean isOpenRaise = false;


    //public GameObject btnCall;

    //public GameObject btnRaise;

    //public GameObject btnFold;

    //public GameObject btnCheck;


    //public Vector3[] playerPos;
    //public Vector3 mainplayerPos;


    private static playerManager _instance;
    public static playerManager Instance
    {
        get
        {
            if (_instance == null)
            {


            }

            return _instance;
        }
    }


    //public List<UnityEngine.GameObject> players = new List<UnityEngine.GameObject>();

    void Awake()
    {


        this.transform.position = Vector3.zero;
        this.transform.localPosition = Vector3.zero;


        foreach (var player in players)
        {
            player.SetActive(false);
        }
        
        foreach (var card in tableCard)
        {
            card.SetActive(false);
        }

 
        networkManager.Instance.protocolInGame = (string push, string data) =>
        {
            //dynamic result = JObject.Parse(data);

            Debug.Log(data);

            QuickType.InGame.Temperatures inGame = JsonConvert.DeserializeObject<QuickType.InGame.Temperatures>(data);

            if (inGame.Push == "needBet") {
                //this.state.needBet();
            }

            if (isSameState(inGame.Data.RoomState) == false) 
            {
                this.setState(stateBase.MakeState(inGame.Data.RoomState, data));
            }

            this.state.SetMessage(inGame.Push, data);

        };

        networkManager.Instance.SetReceive("leaveRoom", (string data) =>
        {
            Debug.Log(data);

        });

        networkManager.Instance.SetReceive("bet", (string data) =>
        {
            Debug.Log(data);

        });


        networkManager.Instance.SetReceive("fold", (string data) =>
        {
            Debug.Log(data);

        });

        networkManager.Instance.SetReceive("raise", (string data) =>
        {
            Debug.Log(data);

        });

        networkManager.Instance.SetReceive("call", (string data) =>
        {
            Debug.Log(data);

        });

        this.setState(stateBase.MakeState("join", ""));

        _instance = this;
    }

    public bool isSameState(string stateName)
    {

        if (state == null || state.name != stateName)
        {
            return false;
        }
        return true;

    }




    public void setState(stateBase _state) 
    {
        if (state != null) {
            state.end(this);
        }
        state = _state;
         _state.begin(this);
    }


    // Update is called once per frame
    void Update()
    {
        if (state != null) {
            state.progress(this);
        }


    }

    public void DoCall() 
    {
      
        Debug.Log("DoCall");

        UIAnimator animator = this.betCart.GetComponent<UIAnimator>();
        animator.PlayAnimation(AnimSetupType.Outro);

        UIAnimator raise = this.animationRaisePOP.GetComponent<UIAnimator>();
        raise.PlayAnimation(AnimSetupType.Outro);



        networkManager.Instance.Send("bet", protocol.doBet(infoManager.Instance.joinRoomIdx, "call", 0), (data) => { 
        
        
        });



    }


    public void DoRaise()
    {
        Debug.Log("DoRaise");

        UIAnimator animator = this.animationRaisePOP.GetComponent<UIAnimator>();

        if (animator.IsPlaying == false)
        {
            animator.PlayAnimation(AnimSetupType.Intro);


            this.raiseSlider.value = 0;
            this.raiseSlider.maxValue = infoManager.Instance.mainUser.money;
        }
        else 
        {
            networkManager.Instance.Send("bet", protocol.doBet(infoManager.Instance.joinRoomIdx, "raise", 10), (data) => {


            });

        }


        //UIAnimator raise = this.animationRaisePOP.GetComponent<UIAnimator>();
        //raise.PlayAnimation(AnimSetupType.Outro);

        //networkManager.Instance.Send("bet", protocol.doBet(infoManager.Instance.joinRoomIdx, "raise", 0), (data) => {


        //});


    }

    public void DoFold()
    {
        Debug.Log("DoFold");

        UIAnimator animator = this.betCart.GetComponent<UIAnimator>();
        animator.PlayAnimation(AnimSetupType.Outro);

        UIAnimator raise = this.animationRaisePOP.GetComponent<UIAnimator>();
        raise.PlayAnimation(AnimSetupType.Outro);


        networkManager.Instance.Send("bet", protocol.doBet(infoManager.Instance.joinRoomIdx, "fold", 0), (data) => {


        });

    }
    public void DoCheck()
    {
        Debug.Log("DoCheck");


        UIAnimator animator = this.betCart.GetComponent<UIAnimator>();
        animator.PlayAnimation(AnimSetupType.Outro);

        UIAnimator raise = this.animationRaisePOP.GetComponent<UIAnimator>();
        raise.PlayAnimation(AnimSetupType.Outro);

        networkManager.Instance.Send("bet", protocol.doBet(infoManager.Instance.joinRoomIdx, "check", 0), (data) => {


        });

    }

    public void OnRaiseBalanceChanged(Slider slider)
    {
        betBalanceView.GetComponent<Text>().text = string.Format("{0:n0}", slider.value);
    }

}
    
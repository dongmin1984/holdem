﻿using GameDevWare.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using UnityEngine;

public class protocol 
{

    public static string login(string ID, string PW)
    { 
        return Json.SerializeToString(new { func = "login", data = new {account = ID,password = PW}});
    }


    public static string userinfo()
    {
        return Json.SerializeToString(new { func = "userInfo"});
    }

    public static string getRooms()
    {
        return Json.SerializeToString(new { func = "getRooms"});
    }


    public static string enterRoom(long roomId)
    {
        return Json.SerializeToString(new { func = "enterRoom" , data = new { roomId = roomId } });
    }

    public static string doBet(long roomId, string betType, int betBalance)
    {
        return Json.SerializeToString(new { func = "bet", data = new { roomId = roomId, betType = betType, betBalance = betBalance } });
    }
    

}

﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

class resourcesManager
{


    public Dictionary<string, Texture2D> textureMrg = new Dictionary<string, Texture2D>();

    private static resourcesManager _instance;
    public static resourcesManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new resourcesManager();


            }

            return _instance;
        }
     }


    public Texture2D FindTexture(string name) 
    {



        string folderpath = Application.dataPath + @"/Resources/textrue/card2/" + name;

        //var texture = Resources.Load<Texture2D>("/textrue/card/" + name);


        if (File.Exists(folderpath) && textureMrg.ContainsKey(name) == false)
        {
            byte[] bytes = File.ReadAllBytes(folderpath);
            Texture2D result = new Texture2D(2, 2);
            result.LoadImage(bytes);

            textureMrg[name] = result;

        }

        
        //if (textureMrg.ContainsKey(name) == false) {

        //    //textureMrg[name] = Resources.Load("textrue/card/" + name) as Sp rite;
        //    //Sprite[] sprites  = Resources.LoadAll("/textrue/card/");

        //    Object[] textures = Resources.LoadAll(folderpath);

        //    //var imageTextAsset = Resources.Load("textrue/card/" + name);


        //}



        //string filePath = "\\data\\graphics\\16BitSoftLogo.png";
        //Texture2D texture = null;
        //byte[] fileData;

        //if (File.Exists(filePath))
        //{
        //    fileData = File.ReadAllBytes(filePath);
        //    texture = new Texture2D(2, 2);
        //    texture.LoadImage(fileData);
        //}

        //Sprite sprite = new Sprite();
        //sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2, texture.height / 2));

        return textureMrg[name];
    }

}

﻿using GameDevWare.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


class EventError
{

    public string error;
}

public class protocolreceive
{

    public static bool IsError(string ms)
    {
        return ms.IndexOf("error") > -1;
    }


    public static user ConvertToUser(string ms) 
    {

        QuickType.User.Temperatures result = JsonConvert.DeserializeObject<QuickType.User.Temperatures>(ms);

        if (result.Data != null) 
        {
        
        }
        var user = new user();
        user.account = result.Data.User.Account;
        user.money = result.Data.User.Money;
        user.id = result.Data.User.Id;



        return user;

    }

    public static List<Room> ConvertToRoom(string ms)
    {

        QuickType.Rooms.Temperatures    result = JsonConvert.DeserializeObject<QuickType.Rooms.Temperatures>(ms);

        List<Room> rooms = new List<Room>();

        foreach (var data in result.Data.Rooms)
        {

            Room kRoom = new Room();
            kRoom.id = data.Id;
            kRoom.name = data.Name;
            foreach (var slot in data.Slots)
            {
                //UnityEngine.Debug.Log(typeof(slot));

                if (slot != null && slot.User != null) {

                    var user = new tableUser();
                    user.id = slot.User.Id;
                    user.account = slot.User.Account;
                    user.money = slot.User.Money;
                    user.state = slot.State;


                    kRoom.slots[user.account.ToString()] = user;
                }
            }
            rooms.Add(kRoom);
        }

        return rooms;
    }

    public static Room ConvertToJoinRoom(string ms, string userID)
    {


        QuickType.JoinRoom.Welcome welcome = QuickType.JoinRoom.Welcome.FromJson(ms);

        Debug.Log(ms);



        Room kRoom = new Room();
        kRoom.id = welcome.Data.Room.Id;
        kRoom.name = welcome.Data.Room.Name.ToString();
        kRoom.state= welcome.Data.Room.State.ToString();

        int tableNumber = 0;
        int chairNumber = 0;
        foreach (var slot in welcome.Data.Room.Slots)
        {
            if (slot != null) {
                var user = new tableUser();
                user.id = slot.User.Id;
                user.account = slot.User.Account;
                user.money = slot.User.Money;
                user.state = slot.State;
                user.chairNumber = tableNumber;

                kRoom.slots[user.account.ToString()] = user;


                if (userID == slot.User.Account.ToString())
                {
                    chairNumber = tableNumber;
                }
            }

            tableNumber++;
        }


        foreach(var slot in kRoom.slots)
        {
            slot.Value.gameNumber = slot.Value.chairNumber - chairNumber >= 0 ? slot.Value.chairNumber - chairNumber  : tableNumber - chairNumber + slot.Value.chairNumber ;
        }

        kRoom.tableCards = new TableCard[3];
        for (int i = 0; i < 3; i++)
        {
            if (welcome.Data.Room.TableCards.Length > i) {
                var card = welcome.Data.Room.TableCards[i];
                if (card != null)
                {
                    var kCard = new TableCard();
                    kCard.value = card.Value;
                    kCard.suit = card.Suit;
                    kCard.numericValue = card.NumericValue;

                    kRoom.tableCards[i] = kCard;
                }
            }
        }


        return kRoom;
    }



}

﻿using GameDevWare.Serialization;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using WebSocketSharp;

public class Even {
    public String func;
    public String push;
    public String error;
}


public class networkManager 
{
    public delegate void receiveSuccess(string data);
    public delegate void receiveGame(string push, string data);


    public WebSocket ws;
    public Dictionary<string, receiveSuccess> protocolPromises = new Dictionary<string, receiveSuccess>();
    public receiveGame protocolInGame = null;



    private static networkManager _instance;
    public static networkManager Instance
    {
        get
        {
            if (_instance  == null)
            {
                //_instance = GameObject.FindObjectOfType(typeof(networkManager));
                //if (_instance == null)
                //{
                //    GameObject container = new GameObject();
                //    container.name = "MyClassContainer";
                    _instance = new networkManager();


                //}

            }

            return _instance;
        }
    }

    private networkManager() {

        
        ws = new WebSocket("ws://localhost:3000/");
        //ws = new WebSocket("ws://18.183.17.24:3000/");
        ws.EmitOnPing = false;

        ws.OnClose += (sender, e) => {
            Debug.Log("OnClose ");
        };


        ws.OnError += (sender, e) => {
            Debug.Log("OnError ");

        };

        ws.OnMessage += (sender, e) => {
            try
            {
                if (e.IsBinary)
                {
                }
                else if (e.IsPing)
                {
                }
                else if (e.IsText && e.Data != "hello")
                {
                    Even even = Json.Deserialize<Even>(e.Data);


                    if (even.func != null && protocolPromises.ContainsKey(even.func))
                    {
                        protocolPromises[even.func](e.Data);
                    }
                    else if (even.push  != null &&  even.push != "" && protocolInGame != null)
                    {


                        protocolInGame(even.push,e.Data);
                    }

                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);

            }


        };
        ws.Connect();
    }


    private void websocketOpend(object sender, EventArgs e)
    {
        Debug.Log("websocketOpend");
    }
    private void websocketError(object sender, EventArgs e)
    {
        Debug.Log("websocketError");
    }
   
    private void websocketClosed(object sender, EventArgs e)
    {
        Debug.Log("websocketClosed");

    }


    public void Send(string even, string message, receiveSuccess func) 
    {
        this.protocolPromises[even] = func;
        this.ws.Send(message);
    }


    public void SetReceive(string even, receiveSuccess func)
    {
        this.protocolPromises[even] = func;
    }
}



//static async Task void <T>(ref T lhs, ref T rhs)
//{

//    Task.Run(() => {
//    })

//    //networkManager.

//}

//public class Task<T>
//{
//    void DoWork<T>() { 

//    }
//}
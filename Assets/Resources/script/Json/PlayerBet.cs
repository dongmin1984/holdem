﻿namespace QuickType.PlayerBet
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Temperatures
    {
        [JsonProperty("push")]
        public string Push { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("roomId")]
        public long RoomId { get; set; }

        [JsonProperty("roomState")]
        public string RoomState { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("betType")]
        public string BetType { get; set; }


        [JsonProperty("betBalance")]
        public int betBalance { get; set; }

        [JsonProperty("totalBetBalance")]
        public int TotalBetBalance { get; set; }
    }

    public partial class User
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("money")]
        public int Money { get; set; }

        [JsonProperty("savemoney")]
        public int Savemoney { get; set; }

        [JsonProperty("point")]
        public int Point { get; set; }
    }
}

﻿namespace QuickType.User
{
        using System;
        using System.Collections.Generic;

        using System.Globalization;
        using Newtonsoft.Json;
        using Newtonsoft.Json.Converters;

        public partial class Temperatures
        {
            [JsonProperty("func")]
            public string Func { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }
        }

        public partial class Data
        {
            [JsonProperty("user")]
            public User User { get; set; }
        }

        public partial class User
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("account")]
            public string Account { get; set; }

            [JsonProperty("money")]
            public long Money { get; set; }

            [JsonProperty("savemoney")]
            public long Savemoney { get; set; }

            [JsonProperty("point")]
            public long Point { get; set; }
        }

}

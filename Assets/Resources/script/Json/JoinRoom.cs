﻿
namespace QuickType.JoinRoom
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Welcome
    {
        [JsonProperty("func")]
        public string Func { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("room")]
        public Room Room { get; set; }
    }

    public partial class Room
    {
        [JsonProperty("slots")]
        public Slot[] Slots { get; set; }

        [JsonProperty("tableCards")]
        public TableCard[] TableCards { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }

    public partial class Slot
    {
        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }

    public partial class User
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("money")]
        public long Money { get; set; }

        [JsonProperty("savemoney")]
        public long Savemoney { get; set; }

        [JsonProperty("point")]
        public long Point { get; set; }
    }

    public partial class TableCard
    {
        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("suit")]
        public int  Suit { get; set; }

        [JsonProperty("numericValue")]
        public int  NumericValue { get; set; }
    }

    public partial class Welcome
    {
        public static Welcome FromJson(string json) => JsonConvert.DeserializeObject<Welcome>(json, QuickType.JoinRoom.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Welcome self) => JsonConvert.SerializeObject(self, QuickType.JoinRoom.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
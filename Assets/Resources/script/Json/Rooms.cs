﻿namespace QuickType.Rooms
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Temperatures
    {
        [JsonProperty("func")]
        public string Func { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("rooms")]
        public Room[] Rooms { get; set; }
    }

    public partial class Room
    {
        [JsonProperty("slots")]
        public Slot[] Slots { get; set; }

        [JsonProperty("tableCards")]
        public TableCard[] TableCards { get; set; }

        [JsonProperty("betBalance")]
        public object[] BetBalance { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("dealerSlot")]
        public long DealerSlot { get; set; }

        [JsonProperty("totalBetBalance")]
        public long TotalBetBalance { get; set; }
    }

    public partial class Slot
    {
        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }

    public partial class User
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("money")]
        public long Money { get; set; }

        [JsonProperty("savemoney")]
        public long Savemoney { get; set; }

        [JsonProperty("point")]
        public long Point { get; set; }
    }

    public partial class TableCard
    {
        [JsonProperty("value")]
        public long Value { get; set; }

        [JsonProperty("suit")]
        public long Suit { get; set; }

        [JsonProperty("numericValue")]
        public long NumericValue { get; set; }
    }
}

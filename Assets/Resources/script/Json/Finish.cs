﻿namespace QuickType.Finish
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Temperatures
    {
        [JsonProperty("push")]
        public string Push { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("roomId")]
        public long RoomId { get; set; }

        [JsonProperty("roomState")]
        public string RoomState { get; set; }

        [JsonProperty("winner")]
        public Winner Winner { get; set; }

        [JsonProperty("handRank")]
        public long HandRank { get; set; }

        [JsonProperty("userCards")]
        public UserCard[] UserCards { get; set; }
    }

    public partial class UserCard
    {
        [JsonProperty("first")]
        public First First { get; set; }

        [JsonProperty("second")]
        public First Second { get; set; }

        [JsonProperty("account")]
        public string Account { get; set; }
    }

    public partial class First
    {
        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("suit")]
        public int Suit { get; set; }

        [JsonProperty("numericValue")]
        public int NumericValue { get; set; }
    }

    public partial class Winner
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("money")]
        public long Money { get; set; }

        [JsonProperty("savemoney")]
        public long Savemoney { get; set; }

        [JsonProperty("point")]
        public long Point { get; set; }
    }
}

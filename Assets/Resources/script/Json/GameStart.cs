﻿namespace QuickType.GameStart
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Temperatures
    {
        [JsonProperty("push")]
        public string Push { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("roomId")]
        public int RoomId { get; set; }

        [JsonProperty("roomState")]
        public string RoomState { get; set; }

        [JsonProperty("dealerSlot")]
        public int DealerSlot { get; set; }
    }
}

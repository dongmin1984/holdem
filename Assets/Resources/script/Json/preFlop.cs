﻿
namespace QuickType.PreFlop
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Temperatures
    {
        [JsonProperty("push")]
        public string Push { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("roomId")]
        public long RoomId { get; set; }

        [JsonProperty("roomState")]
        public string RoomState { get; set; }

        [JsonProperty("tableCard1")]
        public TableCard TableCard1 { get; set; }

        [JsonProperty("tableCard2")]
        public TableCard TableCard2 { get; set; }

        [JsonProperty("tableCard3")]
        public TableCard TableCard3 { get; set; }
        [JsonProperty("tableCard4")]
        public TableCard TableCard4 { get; set; }
        [JsonProperty("tableCard5")]
        public TableCard TableCard5 { get; set; }


        [JsonProperty("userCard1")]
        public UserCard UserCard1 { get; set; }

        [JsonProperty("userCard2")]
        public UserCard UserCard2 { get; set; }
    }

    public partial class TableCard
    {
        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("suit")]
        public int Suit { get; set; }

        [JsonProperty("numericValue")]
        public int NumericValue { get; set; }
    }


    public partial class UserCard
    {
        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("suit")]
        public int Suit { get; set; }

        [JsonProperty("numericValue")]
        public int NumericValue { get; set; }
    }
}

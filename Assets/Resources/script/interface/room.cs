﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Card
{
    public int value;
    public int suit;
    public int numericValue;
}

public class TableCard : Card
{

}

public class UserCard
{
    public Card card1;
    public Card card2;
}

public class Winner
{
    public long id { get; set; }

    public string account { get; set; }

    public long money { get; set; }

    public long savemoney { get; set; }

    public long point { get; set; }
}



public class Room
{
    public long id;

    public string name;

    public string state;

    public int dealerSlot;

    public Winner winner;

    public TableCard[] tableCards;
    
    public UserCard[] userCard;

    public Dictionary<string, tableUser>  slots = new Dictionary<string, tableUser>();

    public bool IsPreflop() 
    {
        return this.state == "preflop";
    }

    public bool IsFlop()
    {
        return this.state == "flop";
    }


    public bool IsTurn()
    {
        return this.state == "turn";
    }


    public bool IsRiver()
    {
        return this.state == "river";
    }


    public bool IsStarted()
    {
        return this.state == "started";
    }



}
